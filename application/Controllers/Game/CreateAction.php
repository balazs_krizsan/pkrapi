<?php namespace App\Controllers\Game;

use App\Services\Game\GameService;
use Slim\Http\Request;
use Slim\Http\Response;

class CreateAction
{
    /** @var  GameService */
    private $gameService;

    public function __construct(GameService $gameService)
    {
        $this->gameService = $gameService;
    }

    public function __invoke(Request $request, Response $response)
    {
        $players = $request->getParam('player');
        $gameResponse = $this->gameService->create($players);

        return $response->withJson($gameResponse);
    }
}
