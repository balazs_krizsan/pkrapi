<?php namespace App\Repository\Players;

use App\Entities\Player;
use App\Repository\AbstractRepository;
use Illuminate\Database\Capsule\Manager;
use stdClass;

class PlayerRepository extends AbstractRepository
{
    const TABLE_NAME = 'players';

    public function getTableName()
    {
        return self::TABLE_NAME;
    }

    public function getPlayer(int $gameId, int $place): Player
    {
        $playerRow = $this->getTable()->select('*')->where('game_id', '=', $gameId)->where('place', '=', $place)->first();

        return $this->createPlayerEntity($playerRow);
    }

    public function getActivePlayer(int $gameId): Player
    {
        $playerRow = $this->getTable()->select('*')->where('game_id', '=', $gameId)->where('active_player', '=', 1)->first();

        return $this->createPlayerEntity($playerRow);
    }

    public function getPlayersNumber(int $gameId): int
    {
        return $this->getTable()
            ->select(['game_id', Manager::raw('count(game_id) as total_count')])
            ->where('game_id', '=', $gameId)
            ->groupBy('game_id')
            ->first()
            ->total_count;
    }

    public function add(string $playerName, string $playerId, int $gameId, int $place): int
    {
        return $this->getTable()->insertGetId([
            'remote_id' => $playerId,
            'name' => $playerName,
            'game_id' => $gameId,
            'place' => $place,
        ]);
    }

    private function createPlayerEntity(stdClass $player): Player
    {
        return new Player($player->id, $player->game_id, $player->remote_id, $player->place);
    }
}
