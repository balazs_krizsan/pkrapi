<?php namespace App\Repository\CardQueue;

use App\Repository\AbstractRepository;

class CardQueueRepository extends AbstractRepository
{
    const TABLE_NAME = 'card_queue';

    public function getTableName()
    {
        return self::TABLE_NAME;
    }

    public function markRandomCardUsed(int $gameId, int $userId): string
    {
        $card = $this->getTable()->select('card')
            ->where('touched', '=', false)
            ->where('game_id', '=', $gameId)
            ->inRandomOrder()
            ->first()
            ->card;

        $this->getTable()
            ->where('game_id', '=', $gameId)
            ->where('card', '=', $card)
            ->update(['user_id' => $userId, 'touched' => true]);

        return $card;
    }

    public function setup(int $gameId, string $card)
    {
        $this->getTable()->insert(['card' => $card, 'game_id' => $gameId]);
    }
}
