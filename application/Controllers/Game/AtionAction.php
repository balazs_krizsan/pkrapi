<?php namespace App\Controllers\Game;

use App\Services\Game\GameService;
use Slim\Http\Request;
use Slim\Http\Response;

class ActionAction
{
    /** @var  GameService */
    private $gameService;

    public function __construct(GameService $gameService)
    {
        $this->gameService = $gameService;
    }


    public function __invoke(Request $request, Response $response)
    {
        $gameHash = $request->getParam('hash');
        $action = $request->getParam('action');

        $gameId = $this->gameService->getIdByHash($gameHash);

        $nextPlayer = $this->gameService->setAndGetNextPlayer($gameId);

        var_dump($nextPlayer);
        $this->gameService->setActivePlayer();
        return $response->withJson([]);
    }
}
