<?php namespace App\Entities;

use JsonSerializable;

class Player implements JsonSerializable 
{
    /** @var  int */
    private $playerId;

    /** @var  int */
    private $gameId;

    /** @var  string */
    private $remoteId;

    /** @var  int */
    private $place;

    public function __construct(string $playerId, string $gameId, string $remoteId, string $place)
    {
        $this->playerId = (int)$playerId;
        $this->gameId = (int)$gameId;
        $this->remoteId = $remoteId;
        $this->place = (int)$place;
    }

    public function getPlayerId(): int
    {
        return $this->playerId;
    }

    public function getGameId(): int
    {
        return $this->gameId;
    }

    public function getRemoteId(): string
    {
        return $this->remoteId;
    }

    public function getPlace(): int
    {
        return $this->place;
    }
    
    function jsonSerialize()
    {
        return [
            'playerId' => $this->getPlayerId(),
            'gameId' => $this->getGameId(),
            'remoteId' => $this->getRemoteId(),
            'place' => $this->getPlace(),
        ];
    }
}
