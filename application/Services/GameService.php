<?php namespace App\Services\Game;

use App\Config;
use App\Repository\CardQueue\CardQueueRepository;
use App\Repository\GameRepository;
use App\Repository\Players\PlayerRepository;
use App\Services\PlayerService;

class GameService
{
    /** @var PlayerService  */
    private $playerService;

    /** @var  CardQueueRepository */
    private $cardQueueRepository;

    /** @var  PlayerRepository */
    private $playerRepository;

    /** @var GameRepository */
    private $gameRepository;

    /** @var Config */
    private $config;

    public function __construct(
        PlayerService $playerService,
        CardQueueRepository $cardQueueRepository,
        PlayerRepository $playerRepository,
        GameRepository $gameRepository,
        Config $config
    )
    {
        $this->playerService = $playerService;
        $this->cardQueueRepository = $cardQueueRepository;
        $this->playerRepository = $playerRepository;
        $this->gameRepository = $gameRepository;
        $this->config = $config;
    }

    public function create($players) //todo: add ": string"
    {
        $gameHash = sha1(time() . time());

        $gameId = (int)$this->gameRepository->create($gameHash, count($players), 0);

        $this->setupCardQueue($gameId);
        $playerIds = $this->setupPlayers($gameId, $players);
        $cards = $this->selectStartingCards($gameId, $playerIds);
        $playersNumber = $this->playerRepository->getPlayersNumber($gameId);

        $dealer = $this->gameRepository->getDealer($gameHash);
        $bigBlind = $this->playerService->getSmallBlind($gameId, $dealer->getPlace(), $playersNumber);
        $smallBlind = $this->playerService->getBigBlind($gameId, $dealer->getPlace(), $playersNumber);
        $activePlayer = $this->playerService->getActivePlayerOnNewTable($gameId, $dealer->getPlace(), $playersNumber);
        $this->setActivePlayer($gameId, $activePlayer->getPlace());



        return [
            'gameHash' => $gameHash,
            'gameId' => $gameId,
            'playerCards' => $cards,
            'dealer' => $dealer,
            'bigBlind' => $bigBlind,
            'smallBlind' => $smallBlind,
            'activePlayer' => $activePlayer,
        ];
    }

    private function selectStartingCards(int $gameId, array $createdPlayers): array
    {
        $playerCards = [];
        foreach ($createdPlayers as $createdPlayer) {
            $playerCards[$createdPlayer['remote_id']] = [];
            $playerCards[$createdPlayer['remote_id']][] = $this->cardQueueRepository->markRandomCardUsed(
                $gameId, $createdPlayer['id']
            );
            $playerCards[$createdPlayer['remote_id']][] = $this->cardQueueRepository->markRandomCardUsed(
                $gameId, $createdPlayer['id']
            );
        }

        return $playerCards;
    }

    private function setupPlayers(int $gameId, array $players): array
    {
        $createdPlayers = [];

        $place = 0;
        foreach ($players as $player) {
            $playerId = $this->playerRepository->add($player['name'], $player['id'], $gameId, $place++);
            $createdPlayers[] = [
                'id' => $playerId,
                'remote_id' => $player['id'],
            ];
        }

        return $createdPlayers;
    }

    private function setupCardQueue(int $gameId)
    {
        $cards = $this->config->get('cards.list');
        foreach ($cards as $card) {
            $this->cardQueueRepository->setup($gameId, $card);
        }
    }

    public function getIdByHash(string $hash): int
    {
        return $this->gameRepository->getIdByToken($hash);
    }

    public function setAndGetNextPlayer(string $nextPlayer)
    {
        $this->gameRepository->setAndGetNextPlayer();
    }

    public function setActivePlayer(int $gameId, int $activePlayerId)
    {
        $this->gameRepository->setActivePlayer($gameId, $activePlayerId);
    }
}
