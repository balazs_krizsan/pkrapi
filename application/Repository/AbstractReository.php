<?php namespace App\Repository;

use Illuminate\Database\Query\Builder;
use Illuminate\Database\Capsule\Manager;


abstract class AbstractRepository
{
    /** @var  Manager */
    protected $db;

    public function __construct(Manager $db)
    {
        $this->db = $db;
    }

    protected function getTable(): Builder
    {
        return $this->db->table($this->getTableName());
    }

    abstract protected function getTableName();
}
