<?php
// Routes

use App\Controllers\Game\ActionAction;
use App\Controllers\Game\CreateAction;

$app->get('/[{name}]', function ($request, $response, $args) {
    // Sample log message
    $this->logger->info("Slim-Skeleton '/' route");

    // Render index view
    return $this->renderer->render($response, 'index.phtml', $args);
});

$app->post('/game/create', CreateAction::class);
$app->post('/game/action', ActionAction::class);
