<?php namespace App\Repository;

use App\Entities\Player;
use App\Repository\AbstractRepository;

class GameRepository extends AbstractRepository
{
    const TABLE_NAME = 'games';

    public function getTableName()
    {
        return self::TABLE_NAME;
    }

    public function create(string $hash, int $players, int $dealer)
    {
        return $this->getTable()->insertGetId([
            'hash' => $hash,
            'players' => $players,
            'dealer' => $dealer
        ]);
    }

    public function getDealer(string $hash): Player
    {
        $playerRow = $this->getTable()
            ->select('players.*')
            ->join('players', 'players.place', '=', self::TABLE_NAME . '.dealer')
            ->where(self::TABLE_NAME.'.hash', '=', $hash)
            ->first();

        return $this->createPlayerEntity($playerRow);
    }

    public function setActivePlayer(int $gameId, int $activePlayerId)
    {
        $this->getTable()->where('id', '=', $gameId)->update(['active_player' => $activePlayerId]);
    }

setAndGetNextPlayer

    private function createPlayerEntity($player): Player
    {
        return new Player($player->id, $player->game_id, $player->remote_id, $player->place);
    }

    public function getIdByToken(string $hash): int
    {
        return (int)$this->getTable()->select('id')->where('hash', '=', $hash)->limit(1)->first()->id;
    }
}
