<?php

use App\Config;
use App\Controllers\Game\ActionAction;
use App\Controllers\Game\CreateAction;
use App\Repository\CardQueue\CardQueueRepository;
use App\Repository\GameRepository;
use App\Repository\Players\PlayerRepository;
use App\Services\Game\GameService;
use App\Services\PlayerService;
use Illuminate\Database\Capsule\Manager;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use Monolog\Processor\UidProcessor;
use Slim\Container;

$container = $app->getContainer();

$container['renderer'] = function (Container $container) {
    $settings = $container->get('settings')['renderer'];

    return new Slim\Views\PhpRenderer($settings['template_path']);
};

$container['logger'] = function (Container $container) {
    $settings = $container->get('settings')['logger'];
    $logger = new Logger($settings['name']);
    $logger->pushProcessor(new UidProcessor());
    $logger->pushHandler(new StreamHandler($settings['path'], $settings['level']));

    return $logger;
};

$container['db'] = function (Container $container) {
    $capsule = new Manager;
    $capsule->addConnection($container['settings']['db']);

    $capsule->setAsGlobal();
    $capsule->bootEloquent();

    return $capsule;
};

$container[Config::class] = function () {
    return new Config;
};

// CONTROLLERS

$container[CreateAction::class] = function (Container $container) {
    $gameService = $container->get(GameService::class);

    return new CreateAction($gameService);
};

$container[ActionAction::class] = function (Container $container) {
    $gameService = $container->get(GameService::class);

    return new ActionAction($gameService);
};

// SERVICES

$container[GameService::class] = function (Container $container) {
    $playerService = $container->get(PlayerService::class);
    $cardQueueRepository = $container->get(CardQueueRepository::class);
    $playerRepository = $container->get(PlayerRepository::class);
    $gameRepository = $container->get(GameRepository::class);
    $config = $container->get(Config::class);

    return new GameService(
        $playerService,
        $cardQueueRepository,
        $playerRepository,
        $gameRepository,
        $config
    );
};

$container[PlayerService::class] = function (Container $container) {
    $playerRepository = $container->get(PlayerRepository::class);

    return new PlayerService($playerRepository);
};

// REPOSITORIES

$container[GameRepository::class] = function (Container $container) {
    $db = $container->get('db');

    return new GameRepository($db);
};

$container[CardQueueRepository::class] = function (Container $container) {
    $db = $container->get('db');

    return new CardQueueRepository($db);
};

$container[PlayerRepository::class] = function (Container $container) {
    $db = $container->get('db');

    return new PlayerRepository($db);
};
