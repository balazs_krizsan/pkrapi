<?php

use Phinx\Migration\AbstractMigration;

class SiteInit extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->table('games');
        $table->addColumn('hash', 'text')
            ->addTimestamps(null, 'completed_at')->create();

        $table = $this->table('card_queue', ['id' => false]);
        $table->addColumn('card', 'text')
            ->addColumn('game_id', 'integer')
            ->addColumn('user_id', 'integer', ['default' => 0])
            ->addColumn('touched', 'boolean', ['default' => false])
            ->create();

        $table = $this->table('players');
        $table
            ->addColumn('remote_id', 'text')
            ->addColumn('name', 'text')
            ->addColumn('game_id', 'integer')
            ->addColumn('place', 'integer')
            ->create();
    }
}
