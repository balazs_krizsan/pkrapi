<?php namespace App\Services;

use App\Entities\Player;
use App\Repository\Players\PlayerRepository;

class PlayerService
{
    const SMALL_BLIND = 'small_blind';
    const BIG_BLIND = 'big_blind';
    const ACTIVE = 'active';

    const PLACE_FROM_DEALER = [
        self::SMALL_BLIND => 1,
        self::BIG_BLIND => 2,
        self::ACTIVE => 3,
    ];

    /** @var  PlayerRepository */
    private $playerRepository;

    public function __construct(PlayerRepository $playerRepository)
    {
        $this->playerRepository = $playerRepository;
    }

    public function getSmallBlind(int $gameId, int $dealerPlace, int $playersNumber): Player
    {
        return $this->playerRepository->getPlayer($gameId, $this->getPlayerPlace(self::SMALL_BLIND, $dealerPlace, $playersNumber));
    }

    public function getBigBlind(int $gameId, int $dealerPlace, int $playersNumber): Player
    {
        return $this->playerRepository->getPlayer($gameId, $this->getPlayerPlace(self::BIG_BLIND, $dealerPlace, $playersNumber));
    }

    public function getActivePlayerOnNewTable(int $gameId, int $dealerPlace, int $playersNumber): Player
    {
        return $this->playerRepository->getPlayer($gameId, $this->getPlayerPlace(self::ACTIVE, $dealerPlace, $playersNumber));
    }

    public function getActivePlayer(): Player
    {
        return $this->playerRepository->getActivePlayer();
    }

    private function getPlayerPlace(string $position, int $dealerPlace, int $playersNumber): int
    {
        $placeFromDealer = self::PLACE_FROM_DEALER[$position];

        if ($dealerPlace + $placeFromDealer >= $playersNumber) {
            return $dealerPlace + $placeFromDealer - $playersNumber;
        }

        return $dealerPlace + $placeFromDealer;
    }
}